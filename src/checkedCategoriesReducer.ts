import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface CheckedState {
	catID: number;
	checked: boolean;
}

const initialState: CheckedState[] = [];

const checkedCategories = createSlice({
	name: "checkedState",
	initialState,
	reducers: {
		toggleChecked: (state, action: PayloadAction<number>) => {
			const catID = action.payload;
			const index = state.findIndex((item) => item.catID === catID);

			// If the category is found in the state array
			if (index !== -1) {
				// Create a new state array to avoid mutation
				const newState = [...state];

				// Toggle the checked state for the category with the matching catID
				newState[index] = {
					...newState[index],
					checked: !newState[index].checked,
				};
				console.log(newState);

				// Return the new state
				return newState;
			}

			// If the category is not found, return the current state unchanged
			return state;
		},
		initializeState: (_state, action: PayloadAction<[]>) => {
			// Initialize checkedState with default values
			return action.payload;
		},
		resetChecked: (state) => {
			state.forEach((item) => {
				item.checked = false;
			});
		},
	},
});

export const { initializeState, toggleChecked, resetChecked } =
	checkedCategories.actions;

export default checkedCategories.reducer;
