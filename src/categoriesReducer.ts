import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import axios from "axios";
import { RootState } from "./configureStore";

const config = {
	method: "get",
	maxBodyLength: Infinity,
	url:
		import.meta.env.mode === "development"
			? "api/wp-json/wc/v3/products"
			: `${import.meta.env.VITE_API_LINK}/wp-json/wc/v3/products/categories`,
	headers: {
		Authorization: import.meta.env.VITE_API_KEY,
	},
};

export const getCategories = createAsyncThunk(
	"categoriesList/getCategories",
	async () => {
		try {
			const allCategories = await axios.request(config);

			return allCategories;
		} catch (error) {
			console.error("Error fetching all products:", error);
			throw error;
		}
	}
);

export const categoriesSlice = createSlice({
	name: "categories",
	initialState: {
		categories: [] as any,
		isLoading: false,
		hasError: false,
	},
	reducers: {
		// Define your reducers here
		// Example:
		setProducts: (state, action: PayloadAction<any>) => {
			state.categories = action.payload;
		},
		setLoading: (state, action: PayloadAction<boolean>) => {
			state.isLoading = action.payload;
		},
		setError: (state, action: PayloadAction<boolean>) => {
			state.hasError = action.payload;
		},
	},
	extraReducers: (builder) => {
		builder
			.addCase(getCategories.pending, (state) => {
				state.isLoading = true;
				state.hasError = false;
			})
			.addCase(getCategories.fulfilled, (state, action) => {
				state.categories = action.payload.data; // Assuming categories are in a 'data' property
				state.isLoading = false;
				state.hasError = false;
			})
			.addCase(getCategories.rejected, (state) => {
				state.hasError = true;
				state.isLoading = false;
			});
	},
});

// Selectors
export const selectCategories = (state: RootState) => state.categories;
export const selectLoadingState = (state: RootState) =>
	state.products.isLoading;
export const selectErrorState = (state: RootState) => state.categories.hasError;

export default categoriesSlice.reducer;
