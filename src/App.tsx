import "./App.css";
import { Route, BrowserRouter as Router, Routes } from "react-router-dom";
import Shop from "./pages/shop/Shop";
import Product from "./pages/product/Product";
import SuccessPage from "./pages/SuccessPage";
import Contact from "./pages/contact/Contact";

function App() {
	return (
		<Router>
			<Routes>
				<Route path="/" element={<Shop />} />
				<Route path="/product/:id" element={<Product />} />
				<Route path="/success" element={<SuccessPage />} />
				<Route path="/contact" element={<Contact />} />
				<Route path="*" element={<div>Not found</div>} />
			</Routes>
		</Router>
	);
}

export default App;
