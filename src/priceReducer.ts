import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface productBuyAmount {
	minValue: number;
	maxValue: number;
}

const initialState: productBuyAmount = {
	minValue: 0,
	maxValue: 0,
};

export const price = createSlice({
	name: "product",
	initialState,
	reducers: {
		update: (state, action: PayloadAction<number[]>) => {
			state.minValue = action.payload[0];
			state.maxValue = action.payload[1];
		},
		reset: (state) => {
			state.minValue = 1;
			state.maxValue = 200;
		},
	},
});

export const { update, reset } = price.actions;

export default price.reducer;
