import { configureStore } from "@reduxjs/toolkit";
import { combineReducers } from "redux";
import productReducer from "./productReducer";
import productsReducer from "./productsReducer";
import priceReducer from "./priceReducer";
import categoriesReducer from "./categoriesReducer";
import checkedCategoriesReducer from "./checkedCategoriesReducer";

const rootReducer = combineReducers({
	product: productReducer,
	products: productsReducer,
	price: priceReducer,
	categories: categoriesReducer,
	checkedCategories: checkedCategoriesReducer,
});

export const store = configureStore({
	reducer: rootReducer,
	middleware: (getDefaultMiddleware) =>
		getDefaultMiddleware({
			serializableCheck: false,
		}),
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
