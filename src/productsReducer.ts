import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ItemType } from "./types/StoreItemTypes";
import axios from "axios";
import { RootState } from "./configureStore";

const config = {
	method: "get",
	maxBodyLength: Infinity,
	url:
		import.meta.env.mode === "development"
			? "api/wp-json/wc/v3/products"
			: `${import.meta.env.VITE_API_LINK}/wp-json/wc/v3/products`,
	headers: {
		Authorization: import.meta.env.VITE_API_KEY,
	},
};

async function fetchFirstPage() {
	try {
		const response = await axios.request({
			...config,
			params: { per_page: 100, page: 1 },
		});

		return response.data;
	} catch (error) {
		console.error("Error fetching first page:", error);
		throw error;
	}
}

//rework
async function fetchAllProducts() {
	const perPage = 100;
	let allProducts: any[] = [];
	let currentPage = 2;

	try {
		const firstPage = await fetchFirstPage();
		allProducts = [...firstPage];

		while (true) {
			const response = await axios.request({
				...config,
				params: { per_page: perPage, page: currentPage },
			});

			if (response.data.length === 0) {
				// Break the loop if no more products are returned
				break;
			}

			allProducts = [...allProducts, ...response.data]; // Append products to the array
			currentPage++; // Move to the next page
		}
		console.log(allProducts);

		return allProducts;
	} catch (error) {
		console.error("Error fetching products:", error);
		throw error;
	}
}

export const getProducts = createAsyncThunk(
	"productList/getProducts",
	async () => {
		try {
			const allProducts = await fetchAllProducts();

			return allProducts;
		} catch (error) {
			console.error("Error fetching all products:", error);
			throw error;
		}
	}
);

export const productsSlice = createSlice({
	name: "products",
	initialState: {
		products: [] as ItemType[],
		isLoading: false,
		hasError: false,
	},
	reducers: {
		// Define your reducers here
		// Example:
		setProducts: (state, action: PayloadAction<any>) => {
			state.products = action.payload;
		},
		setLoading: (state, action: PayloadAction<boolean>) => {
			state.isLoading = action.payload;
		},
		setError: (state, action: PayloadAction<boolean>) => {
			state.hasError = action.payload;
		},
	},
	extraReducers: (builder) => {
		builder
			.addCase(getProducts.pending, (state) => {
				state.isLoading = true;
				state.hasError = false;
			})
			.addCase(getProducts.fulfilled, (state, action) => {
				if (action.payload) {
					state.products = action.payload;
				}
				state.isLoading = false;
				state.hasError = false;
			})
			.addCase(getProducts.rejected, (state) => {
				state.hasError = true;
				state.isLoading = false;
			});
	},
});

// Selectors
export const selectProducts = (state: RootState) => state.products;
export const selectLoadingState = (state: RootState) =>
	state.products.isLoading;
export const selectErrorState = (state: RootState) => state.products.hasError;

export default productsSlice.reducer;
