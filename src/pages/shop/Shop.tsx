import { useCallback, useEffect, useMemo, useState } from "react";
import Header from "../../components/header/Header";
import StoreItems from "../../components/storeItems/StoreItems";
import { useDispatch, useSelector } from "react-redux";
import SideMenu from "../../components/sideMenu/SideMenu";
import { getProducts, selectLoadingState } from "../../productsReducer";
import _ from "lodash";
import { getCategories } from "../../categoriesReducer";
import { reset } from "../../priceReducer";
import "./Shop.css";

const Shop = () => {
	const dispatch = useDispatch();
	const loading = useSelector(selectLoadingState);
	const [search, setSearch] = useState("");
	const [isMenuOpened, setIsMenuOpened] = useState(false);
	// Memoize the dispatch function
	const dispatchGetProducts = useCallback(() => {
		dispatch(getProducts() as any);
		dispatch(getCategories() as any);
	}, [dispatch]);

	// Memoize the memoized dispatch function
	const memoizedDispatchGetProducts = useMemo(
		() => dispatchGetProducts,
		[dispatchGetProducts]
	);

	useEffect(() => {
		dispatch(reset());
	}, [dispatch]);

	useEffect(() => {
		// Dispatch getProducts only if it's not already loading
		if (!loading) {
			memoizedDispatchGetProducts();
		}
	}, [memoizedDispatchGetProducts]);

	return (
		<>
			<Header />
			<div className="shop-container">
				<SideMenu setSearch={setSearch} />
				<div className="filter-container">
					<button
						className="filters-menu"
						onClick={() => setIsMenuOpened(true)}
					>
						Filteri i pretraga
					</button>
				</div>
				<StoreItems search={search} />
			</div>
			{isMenuOpened && (
				<SideMenu
					setSearch={setSearch}
					isMenuOpened={isMenuOpened}
					setIsMenuOpened={setIsMenuOpened}
				/>
			)}
		</>
	);
};

export default Shop;
