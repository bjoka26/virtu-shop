import Header from "../components/header/Header";
import { IoCheckmarkCircleOutline } from "react-icons/io5";
import { useNavigate } from "react-router-dom";

const SuccessScrean = () => {
	const navigate = useNavigate();
	return (
		<>
			<Header />
			<div className="success">
				<p className="success-icon">
					<IoCheckmarkCircleOutline />
				</p>
				<h1>Hvala na kupovini!</h1>
				<p>Dostavu mozete ocekivati u roku od 1-2 dana</p>
				<button onClick={() => navigate("/")}>Vratite se na pocetak</button>
			</div>
		</>
	);
};

export default SuccessScrean;
