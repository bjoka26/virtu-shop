import { useState } from "react";
import Header from "../../components/header/Header";
import { useLocation } from "react-router-dom";
import ImageSlider from "../../components/imageSlider/ImageSlider";
import safeAndSecureLogo from "../../assets/safeandsecure.png";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../configureStore";
import { decrement, increment } from "../../productReducer";
import "./Product.css";
import Checkout from "../../components/checkout/Checkout";
import { ItemType } from "../../types/StoreItemTypes";
import { Skeleton } from "@mui/material";

const Product = () => {
	const location = useLocation();
	const product = location?.state as ItemType; // Assuming ProductType is the type of your product data

	const [menuOpened, setMenuOpened] = useState(false);
	const [buyModal, setBuyModal] = useState(false);
	const amount = useSelector((state: RootState) => state.product.value);
	const dispatch = useDispatch();

	const buyHandler = () => {
		// dispatch(update(item));
		setBuyModal(true);
	};

	// Remove <p> from the beginning and </p> from the end of the string

	const trimmedString = product.description.replace(/(<([^>]+)>)/gi, "");

	if (!product) {
		// Render Skeleton component while loading
		return (
			<div className="product-page-container">
				<Skeleton
					variant="rectangular"
					width={500}
					height={400}
					animation="wave"
				/>
				<div className="product-info">
					<Skeleton variant="text" width={200} animation="wave" />
					<Skeleton variant="text" width={100} animation="wave" />
					<div className="buy-container">
						<Skeleton variant="text" width={50} animation="wave" />
						<Skeleton variant="text" width={50} animation="wave" />
						<Skeleton variant="text" width={50} animation="wave" />
						<Skeleton variant="text" width={100} animation="wave" />
					</div>
					<div className="secure-gateway">
						<Skeleton variant="text" width={150} animation="wave" />
					</div>
					<div className="delivery-time">
						<Skeleton variant="text" width={150} animation="wave" />
					</div>
					<div className="description-container">
						<Skeleton variant="text" width={100} animation="wave" />
						<Skeleton variant="text" width={300} animation="wave" />
					</div>
				</div>
			</div>
		);
	}

	return (
		<>
			<Header />
			<div className="product-page-container">
				<div className="carousel-img" onClick={() => setMenuOpened(true)}>
					{product.images.map((image, index) => {
						return (
							<div
								className={index === 0 ? "thumbnail-image" : "carousel-image"}
								style={{ backgroundImage: `url('${image.src}')` }}
								key={index}
							></div>
						);
					})}
				</div>
				<div className="product-info">
					<h1>{product.name}</h1>
					<h2>Cijena: {product.price} €</h2>
					<div className="buy-container">
						<h3>Kolicina:</h3>
						<div className="amount-to-buy-container">
							<p onClick={() => dispatch(decrement())}>-</p>
							<p>{amount}</p>
							<p onClick={() => dispatch(increment())}>+</p>
						</div>
						<button onClick={buyHandler}>Kupi sad!</button>
					</div>
					<div className="secure-gateway">
						<img src={safeAndSecureLogo} alt="" />
					</div>
					<div className="delivery-time">
						<h3>Dostava</h3>
						<p>
							Proizvod stiže na vašu adresu za 1-2 dana. Cijena dostave nije
							uracunata u cijenu i iznosi <b>3 eura.</b>
						</p>
					</div>
					{trimmedString !== "" && (
						<div className="description-container">
							<h3>Description</h3>
							<p>{trimmedString}</p>
						</div>
					)}
				</div>
			</div>
			{menuOpened && (
				<ImageSlider images={product.images} setMenuOpened={setMenuOpened} />
			)}
			{buyModal && <Checkout setModal={setBuyModal} product={product} />}
		</>
	);
};

export default Product;
