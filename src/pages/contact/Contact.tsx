import { SubmitHandler, useForm } from "react-hook-form";
import Header from "../../components/header/Header";
import "./Contact.css";

type FormValues = {
	firstAndLastName: string;
	eMail: number;
	note: string;
};

const Contact = () => {
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm<FormValues>();

	// backend needed
	const onSubmit: SubmitHandler<FormValues> = async (_data) => {};

	return (
		<>
			<Header />
			<div className="contact-page-container">
				<h1>Kontaktirajte Nas</h1>
				<form onSubmit={handleSubmit(onSubmit)} className="form">
					<h4>Vasi Podaci</h4>
					<input
						placeholder="Ime i Prezime"
						className="input"
						{...register("firstAndLastName", { required: true })}
					/>
					{errors?.firstAndLastName && <p>Morate popuniti ovo polje</p>}

					<input
						placeholder="E-Mail"
						className="input"
						{...register("eMail", { required: true })}
					/>
					{errors?.eMail && <p>Morate popuniti ovo polje</p>}

					<textarea
						placeholder="Pitanje..."
						className="input"
						{...register("note")}
					/>
					{errors?.note && <p>Morate popuniti ovo polje</p>}

					<button type="submit">Poruci</button>
				</form>
			</div>
		</>
	);
};

export default Contact;
