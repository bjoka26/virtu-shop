import { SetStateAction, useEffect, useState } from "react";
import PriceSlider from "../slider/PriceSlider";
import "./SideMenu.css";
import Box from "@mui/material/Box";
import Checkbox from "@mui/material/Checkbox";
import FormControlLabel from "@mui/material/FormControlLabel";
import Typography from "@mui/joy/Typography";
import { useDispatch, useSelector } from "react-redux";
import { selectProducts } from "../../productsReducer";
import { selectCategories } from "../../categoriesReducer";
import { Category } from "../../types/StoreItemTypes";
import { initializeState, toggleChecked } from "../../checkedCategoriesReducer";
import { MdOutlineCancel } from "react-icons/md";
import { Skeleton } from "@mui/material";

export type CheckedState = {
	catID: number;
	checked: boolean;
};

interface Props {
	setSearch: React.Dispatch<SetStateAction<string>>;
	isMenuOpened?: boolean;
	setIsMenuOpened?: React.Dispatch<SetStateAction<boolean>>;
}

const SideMenu = ({
	setSearch,
	isMenuOpened = false,
	setIsMenuOpened = undefined,
}: Props) => {
	const getProducts = useSelector(selectProducts);
	const categoriesList = useSelector(selectCategories);

	const dispatch = useDispatch();

	const { products } = getProducts;
	const { categories, isLoading } = categoriesList;

	const [maxValue, setMaxValue] = useState(999);
	const [checkedState, setCheckedState] = useState<CheckedState[]>(
		categories.map((category: Category) => ({
			catID: category.id,
			checked: false,
		}))
	);

	// Function to handle checkbox change
	const handleChange = (catID: number) => {
		setCheckedState((prevCheckedState) => {
			// Map through the previous checked state array
			return prevCheckedState.map((item) => {
				// Toggle the checked state if the category ID matches
				if (item.catID === catID) {
					return { ...item, checked: !item.checked };
				}
				return item;
			});
		});

		// Dispatch action to toggle checked state
		dispatch(toggleChecked(catID));
	};

	useEffect(() => {
		if (!categories || !categories.length) {
			return;
		}

		// Initialize checked state with categories
		const initialCheckedState = categories.map((category: Category) => ({
			catID: category.id,
			checked: false,
		}));

		setCheckedState(initialCheckedState);

		dispatch(initializeState(initialCheckedState));
	}, [categories, dispatch, categoriesList]);

	useEffect(() => {
		if (!products || !products.length) {
			// Handle the case where there are no products or products is not yet loaded
			return;
		}

		const maxPrice = products.reduce((maxPrice, product) => {
			const price = +product?.price || 0;
			return price > maxPrice ? price : maxPrice;
		}, +products[0]?.price || 0);

		setMaxValue(maxPrice);
	}, [products]);

	const skeletons = () => {
		const skeletonItems = [];

		for (let index = 0; index < 6; index++) {
			skeletonItems.push(
				<div className="flex-items-row">
					<Skeleton
						variant="text"
						animation="wave"
						sx={{ width: 30, height: 40 }}
					/>
					<Skeleton
						variant="text"
						animation="wave"
						sx={{ width: 220, height: 30 }}
					/>
				</div>
			);
		}

		return skeletonItems;
	};

	return (
		<div
			className={
				isMenuOpened
					? "side-menu-container active"
					: "side-menu-container desktop"
			}
		>
			{isMenuOpened && setIsMenuOpened && (
				<MdOutlineCancel
					className="exit-btn"
					onClick={() => setIsMenuOpened(false)}
				/>
			)}
			<div className="search">
				<input onChange={(e) => setSearch(e.target.value)} type="text" />
				<button>Pretraga</button>
			</div>
			<div className="price-filter">
				<PriceSlider maxValue={maxValue} />
			</div>
			<Box
				sx={{
					display: "flex",
					flexDirection: "column",
					ml: 0,
					gap: 1,
				}}
			>
				<h3>Kategorije</h3>
				{!isLoading
					? categories.map((category: Category, index: number) => (
							<FormControlLabel
								key={index}
								label={
									<Typography
										sx={{ pl: 1, ml: 0, fontSize: 13, fontWeight: 400 }}
									>
										{category.name}
									</Typography>
								}
								control={
									<Checkbox
										size="small"
										sx={{ padding: 0, ml: 1.5 }}
										checked={
											checkedState.find((item) => item.catID === category.id)
												?.checked || false
										}
										onChange={() => handleChange(category.id)}
									/>
								}
							/>
					  ))
					: skeletons()}
			</Box>
		</div>
	);
};

export default SideMenu;
