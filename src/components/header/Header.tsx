import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import "./Header.css";
import { IoChevronBackCircleSharp } from "react-icons/io5";

import { RiMenu2Fill } from "react-icons/ri";

const Header: React.FC = () => {
	const [isMenuOpened, setMenuOpened] = useState(false);
	const navigate = useNavigate();
	return (
		<>
			<header className="header-container">
				<nav className="nav-menu">
					<ul>
						<Link to="/">Prodavnica</Link>
					</ul>
				</nav>
				<div className="logo" onClick={() => navigate("/")}></div>
				<div className="header-profile">
					<Link to="/contact">Kontaktirajte Nas</Link>
				</div>
				<RiMenu2Fill
					className="hamburger-menu"
					onClick={() => setMenuOpened(true)}
				/>
			</header>
			<div className={`${isMenuOpened ? "menu-opened active" : "menu-opened"}`}>
				<IoChevronBackCircleSharp
					className="exit-menu"
					onClick={() => setMenuOpened(false)}
				/>

				<ul>
					<Link to="/">Prodavnica</Link>
					<Link to="/">Kontaktirajte Nas</Link>
				</ul>
			</div>
		</>
	);
};

export default Header;
