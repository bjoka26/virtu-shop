import { Dispatch, SetStateAction, useState } from "react";
import "./ImageSlider.css";
import { Image } from "../../types/StoreItemTypes";
import { BsArrowLeftCircle, BsArrowRightCircle } from "react-icons/bs";

interface SliderProps {
	setMenuOpened: Dispatch<SetStateAction<boolean>>;
	images: Image[];
}

const ImageSlider = ({ setMenuOpened, images }: SliderProps) => {
	const [chosenImage, setChosenImage] = useState(0);
	const [touchStartX, setTouchStartX] = useState<number | null>(null);

	const calculateImageNumber = (option: string) => {
		const totalImages = images.length;

		if (option === "next") {
			setChosenImage((prevState) => (prevState + 1) % totalImages);
		} else if (option === "prev") {
			setChosenImage(
				(prevState) => (prevState - 1 + totalImages) % totalImages
			);
		}
	};

	const handleTouchStart = (e: React.TouchEvent) => {
		setTouchStartX(e.touches[0].clientX);
	};

	const handleTouchMove = (e: React.TouchEvent) => {
		if (!touchStartX) return;

		const touchEndX = e.touches[0].clientX;
		const touchXDiff = touchStartX - touchEndX;

		if (touchXDiff > 50) {
			// Swipe right to left
			calculateImageNumber("next");
		} else if (touchXDiff < -50) {
			// Swipe left to right
			calculateImageNumber("prev");
		}

		setTouchStartX(null);
	};
	return (
		<div className="open-carousel" onClick={() => setMenuOpened(false)}>
			<div
				onTouchStart={handleTouchStart}
				onTouchMove={handleTouchMove}
				className="modal-content"
				onClick={(e) => {
					// do not close modal if anything inside modal content is clicked
					e.stopPropagation();
				}}
			>
				<p onClick={() => calculateImageNumber("prev")}>
					<BsArrowLeftCircle />
				</p>
				<div
					className="open-carousel-image"
					style={{
						backgroundImage: `url('${images[chosenImage].src}')`,
					}}
				></div>
				<p onClick={() => calculateImageNumber("next")}>
					<BsArrowRightCircle />
				</p>
			</div>
		</div>
	);
};

export default ImageSlider;
