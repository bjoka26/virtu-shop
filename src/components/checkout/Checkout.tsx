import React, { SetStateAction, useEffect, useState } from "react";
import { RootState } from "../../configureStore";
import { useSelector } from "react-redux";
import { useForm, SubmitHandler } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { MdOutlineCancel } from "react-icons/md";
import "./Checkout.css";
import { ItemType } from "../../types/StoreItemTypes";
import axios from "axios";
import Loading from "../loading/Loading";

interface Props {
	setModal: React.Dispatch<SetStateAction<boolean>>;
	product: ItemType;
}

type FormValues = {
	firstName: string;
	lastName: string;
	streetName: string;
	city: string;
	phoneNumber: number;
	note: string;
};

const Checkout = ({ setModal, product }: Props) => {
	const item = useSelector((state: RootState) => state.product);
	const [isLoading, setIsLoading] = useState(false);
	const [error, setError] = useState(false);
	const { value } = item;
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm<FormValues>();
	const navigate = useNavigate();

	useEffect(() => {
		window.scrollTo(0, 0);
	}, []);

	const onSubmit: SubmitHandler<FormValues> = async (data) => {
		const product_data = {
			payment_method: "cod",
			payment_method_title: "Platite pouzecem",
			set_paid: true,
			customer_note: data.note,
			billing: {
				first_name: data.firstName,
				last_name: "",
				address_1: data.streetName,
				address_2: "",
				city: data.city,
				state: "",
				postcode: "",
				country: "Crna Gora",
				email: "bojan@virtushop.me",
				phone: data.phoneNumber,
			},
			shipping: {
				first_name: "",
				last_name: "",
				address_1: "",
				address_2: "",
				city: "",
				state: "",
				postcode: "",
				country: "",
			},
			line_items: [
				{
					product_id: product.id,
					quantity: value,
				},
			],
		};

		const config = {
			method: "post",
			maxBodyLength: Infinity,
			url:
				import.meta.env.mode === "development"
					? "api/wp-json/wc/v3/products"
					: `${import.meta.env.VITE_API_LINK}/wp-json/wc/v3/orders`,
			headers: {
				Authorization: import.meta.env.VITE_API_KEY,
			},
			data: product_data,
		};

		try {
			setIsLoading(true);
			const response = await axios.request(config);
			console.log("Order placed successfully:", response);
			navigate("/success");
		} catch (error) {
			setError(true);
			console.error("Error placing order:", error);
		} finally {
			setIsLoading(false);
		}
	};

	return (
		<div className="checkout-container">
			<div className="form-container">
				{isLoading && !error ? (
					<Loading />
				) : (
					<>
						<h2>Narucivanje</h2>
						<MdOutlineCancel
							className="exit-btn"
							onClick={() => setModal(false)}
						/>

						<div className="flex-container">
							<div className="product-checkout-info">
								<div className="image-and-title">
									<div className="checkout-product-image">
										<img src={product.images[0].src} alt="" />
									</div>
									<p>{product.name}</p>
								</div>
								<div className="price-calculation">
									<p>
										Cijena: {product.price} € * {value} ={" "}
										{Math.floor(+product.price * value)} €
									</p>
								</div>
							</div>

							<form onSubmit={handleSubmit(onSubmit)} className="form">
								<h4>Vasi Podaci</h4>
								<input
									placeholder="Ime i Prezime"
									className="input"
									{...register("firstName", { required: true })}
								/>
								{errors?.firstName && <p>Morate popuniti ovo polje</p>}

								<input
									placeholder="Ulica"
									className="input"
									{...register("streetName", { required: true })}
								/>
								{errors?.streetName && <p>Morate popuniti ovo polje</p>}

								<input
									placeholder="Grad"
									className="input"
									{...register("city", { required: true })}
								/>
								{errors?.city && <p>Morate popuniti ovo polje</p>}

								<input
									placeholder="Broj Telefona"
									className="input"
									{...register("phoneNumber", { required: true })}
								/>
								{errors?.phoneNumber && <p>Morate popuniti ovo polje</p>}

								<textarea
									placeholder="Napomena, npr. Dostava posle 17h"
									className="input"
									{...register("note")}
								/>
								{errors?.note && <p>Morate popuniti ovo polje</p>}

								<button type="submit">Poruci</button>
							</form>
						</div>
					</>
				)}
			</div>
		</div>
	);
};

export default Checkout;
