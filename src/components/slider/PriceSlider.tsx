import Box from "@mui/material/Box";
import Slider from "@mui/material/Slider";
import "./Slider.css";
import { useDispatch } from "react-redux";
import { update } from "../../priceReducer";
import { useEffect, useState } from "react";

interface Props {
	maxValue?: number;
}

const PriceSlider = ({ maxValue = 200 }: Props) => {
	const [value, setValue] = useState<number[]>([5, maxValue | 200]);
	const dispatch = useDispatch();

	const handleChange = (_event: Event, newValue: number | number[]) => {
		setValue(newValue as number[]);
	};
	// dispatch(update(value));

	useEffect(() => {
		dispatch(update(value));
	}, [value]);

	return (
		<Box sx={{ width: 280 }}>
			<Slider
				getAriaLabel={() => "Minimum distance"}
				value={value}
				max={199}
				onChange={handleChange}
				disableSwap
			/>
			<div className="price-range-container">
				<label>{value[0]} €</label>
				<label>{value[1]} €</label>
			</div>
		</Box>
	);
};

export default PriceSlider;
