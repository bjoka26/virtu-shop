import "./Loading.css";

const Loading = () => {
	return (
		<div className="loading-div">
			<div className="lds-ring">
				<div></div>
				<div></div>
				<div></div>
				<div></div>
			</div>
			<h3>Loading</h3>
		</div>
	);
};

export default Loading;
