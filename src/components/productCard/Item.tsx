import { Image, ItemType } from "../../types/StoreItemTypes";
import { Link } from "react-router-dom";
import "./Item.css";
import AspectRatio from "@mui/joy/AspectRatio";
import Button from "@mui/joy/Button";
import Card from "@mui/joy/Card";
import CardContent from "@mui/joy/CardContent";
import Typography from "@mui/joy/Typography";

interface StoreItemProps {
	storeItem: ItemType;
	isLoading: boolean;
}

const Item = ({ storeItem }: StoreItemProps) => {
	const product: ItemType = storeItem;
	const image: Image = product.images[0];

	return (
		product && (
			<Link to={`/product/${product.slug}`} state={product}>
				<Card
					sx={{
						width: 250,
						padding: 0,
						border: 0,
						background: "white",
					}}
				>
					<AspectRatio minHeight={250} maxHeight={250}>
						<img src={image?.src} loading="lazy" alt="" />
					</AspectRatio>
					<CardContent orientation="vertical">
						<Typography level="title-sm">{product.name}</Typography>
						<div className="btn-and-price-container">
							<Typography level="body-sm">
								Cijena:{" "}
								<Typography fontSize="sm" fontWeight="lg">
									{product.price} €
								</Typography>
							</Typography>
							<Button
								variant="solid"
								size="md"
								color="primary"
								aria-label="Pogledajte proizvod"
								sx={{ ml: "auto", alignSelf: "center", fontWeight: 600 }}
							>
								Naruci
							</Button>
						</div>
					</CardContent>
				</Card>
			</Link>
		)
	);
};

export default Item;
