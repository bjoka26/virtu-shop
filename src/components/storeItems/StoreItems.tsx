import { useEffect, useMemo, useState } from "react";
import Item from "../productCard/Item";
import "./StoreItems.css";
import { useSelector } from "react-redux";
import { selectProducts } from "../../productsReducer";
import { Card, CardContent, Skeleton } from "@mui/material";
import { ItemType } from "../../types/StoreItemTypes";
import { RootState } from "../../configureStore";

interface Props {
	search: string;
}

const StoreItems = ({ search }: Props) => {
	const productDeps = useSelector(selectProducts);
	const selectedCategories = useSelector(
		(state: RootState) => state.checkedCategories
	);

	const { isLoading, hasError, products } = productDeps;

	const [loadedItems, setLoadedItems] = useState<ItemType[]>([]);
	let number = 0;

	const loadMoreItems = () => {
		const nextItems = products.slice(0, number + 20);
		number = number + 20;
		setLoadedItems(nextItems);
	};

	const handleScroll = () => {
		const distanceToBottom =
			document.documentElement.offsetHeight -
			(window.innerHeight + window.scrollY);

		if (distanceToBottom <= 100 && products.length > number) {
			loadMoreItems();
		}
	};

	useEffect(() => {
		if (products.length > 0) {
			loadMoreItems();
		}

		window.addEventListener("scroll", handleScroll);

		// Clean up event listener on component unmount
		return () => {
			window.removeEventListener("scroll", handleScroll);
		};
	}, [products]);

	const minMax = useSelector(
		useMemo(
			() => (state: RootState) => state.price,
			[] // Empty dependency array ensures that the selector is only created once
		)
	);

	const [filteredProducts, setFilteredProducts] = useState<ItemType[]>([]);

	useEffect(() => {
		let filtered = loadedItems;
		console.log(filtered);

		// Filter products based on selected categories
		const tryToFilter = products.filter((product) =>
			selectedCategories.some(
				(selectedCategory) =>
					selectedCategory.checked &&
					product.categories.some(
						(category) => category.id === selectedCategory.catID
					)
			)
		);

		filtered =
			selectedCategories.length > 0 && tryToFilter.length > 0
				? tryToFilter
				: loadedItems;

		// Apply search filter if search string is provided
		if (search.trim() !== "") {
			filtered = products.filter((product) =>
				product.name.toLowerCase().includes(search.toLowerCase())
			);
		}

		setFilteredProducts(filtered); // Update the state here
	}, [loadedItems, selectedCategories, search]);

	const skeletons = () => {
		const skeletonItems = [];

		for (let index = 0; index < 6; index++) {
			skeletonItems.push(
				<Card key={index} sx={{ width: 250 }}>
					<Skeleton
						variant="rectangular"
						width={250}
						height={200}
						animation="wave"
					/>
					<CardContent>
						<Skeleton variant="text" animation="wave" />
						<Skeleton variant="text" animation="wave" />
						<Skeleton variant="text" animation="wave" />
					</CardContent>
				</Card>
			);
		}

		return skeletonItems;
	};

	return (
		<div className="store-items-container">
			{!isLoading && !hasError && loadedItems.length > 0
				? filteredProducts.map((item, index) => {
						if (
							+item.price >= minMax.minValue &&
							+item.price <= minMax.maxValue
						)
							return (
								<Item key={index} storeItem={item} isLoading={isLoading} />
							);
				  })
				: skeletons()}
		</div>
	);
};

export default StoreItems;
